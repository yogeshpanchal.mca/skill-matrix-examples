<?php
$xmlstr = "<movies>
 <movie>
 <title>PHP: Behind the Parser</title>
  <characters>
   <character>
    <name>Ms. Coder</name>
    <actor>Onlivia Actora</actor>
   </character>
   <character>
    <name>Mr. Coder</name>
    <actor>El Act&#211;r</actor>
   </character>
  </characters>
  <plot>
   So, this language. It's like, a programming language. Or is it a
   scripting language? All is revealed in this thrilling horror spoof
   of a documentary.
  </plot>
  <great-lines>
   <line>PHP solves all my web problems</line>
  </great-lines>
  <rating type='thumbs'>7</rating>
  <rating type='stars'>5</rating>
 </movie>
</movies>";

$movies = new SimpleXMLElement($xmlstr);
echo $movies->movie[0]->plot;
echo '<br>';
echo $movies->movie->{'great-lines'}->line;
echo '<br>';
foreach ($movies->movie->characters->character as $character) {
   echo $character->name, ' played by ', $character->actor, PHP_EOL;
}
echo '<br>';
if ((string) $movies->movie->title == 'PHP: Behind the Parser') {
    echo 'My favorite movie.';
}
echo htmlentities((string) $movies->movie->title);
?>