<?php
	$int = 185;
	$min = 10;
	$max = 100;
	if (filter_var($int, FILTER_VALIDATE_INT, array("options" => array("min_range"=>$min, "max_range"=>$max))) === false) {    
		echo("Variable is not valid");
	}else {    
		echo("Variable is valid");
	}

//PHP Filter Advanced Validate IPv6 Address	
	$ip = "192.168.2.110";
	if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) === false) {  
		echo '<br>';      
		echo("$ip is indeed a valid IPv6 address");
	} else {        
		echo '<br>';
		echo("$ip is no valid IPv6 address");
	}

//PHP Filters Advanced Validate URL – Must Contain QueryString
	$url = "http://www.wolfpacinfo.com";
	if (!filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_QUERY_REQUIRED) === false) {      
		echo '<br>';    
		echo("$url is a valid URL address");
	} else { 
		echo '<br>';  
		echo("$url is no valid URL address");
	}	
//PHP Filters Advanced Remove Characters With ASCII Value > 127
	$str = "<h2>H3110 W0r1dÆØÅ!</h2>";
	$newstring = filter_var($str, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
	echo '<br>';
	echo $newstring;	
?>