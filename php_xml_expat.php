<?php
require('parser.php');
// create parser
$parser = new Parser(
	// start element handler
	function($name,$elementPath,$attribList) {
		echo('Element start: ' . $elementPath . "\n");
		echo '<br>';
		if ($attribList) {
			echo("Attrib list:\n");
			var_dump($attribList);
		}
	},
	// element data handler
	function($elementPath,$data) {
		echo('Element data: ' . $data . "\n");
		echo '<br>';
	}
);
// open XML file for reading
$fh = fopen('php_xml_expat.xml','r');
while (!feof($fh)) {
	// process file in really small chunks
	$parser->process(fread($fh,20));
}
// close file handle and XML parser
fclose($fh);
$parser->close();